import express, { Express, Request, Response } from 'express';
// Security
import cors from 'cors';
import helmet from 'helmet';
// TODO https
// Root Router
import routes from '../routes';
// Swagger
import swaggerUI from 'swagger-ui-express';
import * as swaggerJSON from '../../public/swagger.json';
// MongoDB
import mongoose from 'mongoose';

// Create Express Server
const server: Express = express();

// TODO Mongoose connection
const dbURI = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}`;
mongoose.set('strictQuery', true);
mongoose.connect(dbURI, {
  user: process.env.MONGO_USER,
  pass: process.env.MONGO_PASS,
  dbName: process.env.MONGO_DBNAME
});

// Security configuration
server.use(helmet());
server.use(cors());

// Content type configuration
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

// Define server to use /api and use rootRouter from index.ts in routes
// From this point onover: /api/...
server.use('/api', routes);

// Static server
// server.use(express.static('public'));

// Swagger documentation
server.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

// Redirection from / to /api
server.get('*', (req: Request, res: Response) => {
  res.redirect('/api');
});

export default server;
