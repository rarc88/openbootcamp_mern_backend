import { userEntity } from '../entities/User.entity';
import { LogError } from '../../utils/logger';
import { IUser } from '../interfaces/IUser.interface';
import { IAuth } from '../interfaces/IAuth.interface';

// BCRYPT for passwords
import bcrypt from 'bcrypt';
// JWT
import jwt from 'jsonwebtoken';

// Register user
export const registerUser = async (user: IUser): Promise<any | undefined> => {
  try {
    const userModel = userEntity();
    // Register new user
    return await userModel.create(user);
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user. ${error}`);
  }
};

// Login user
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
  try {
    const userModel = userEntity();

    // Find user by email
    const user = await userModel.findOne({ email: auth.email });

    // Validations

    // Use BCRYPT to compare passwords
    const isValidPassword = bcrypt.compareSync(auth.password, user.password);

    if (!isValidPassword) {
      // TODO -> NOT AUTHORISED (401)
    }

    // Create JWT
    // TODO -> SECRET must be in .env
    const token = jwt.sign({ email: user.email }, 'SECRET', {
      expiresIn: '1d'
    });

    return token;
  } catch (error) {
    LogError(`[ORM ERROR]: Login user. ${error}`);
  }
};

// Logout user
export const logoutUser = async (): Promise<any | undefined> => {};
