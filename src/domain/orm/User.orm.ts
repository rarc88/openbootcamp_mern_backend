import { userEntity } from '../entities/User.entity';
import { LogError } from '../../utils/logger';
import { IUser } from '../interfaces/IUser.interface';

// CRUD

/**
 * Method to obtain all users from collection "Users" in mongo server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity();
    // Search all users
    return await userModel.find({ isDelete: false });
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all users. ${error}`);
  }
};

/**
 * Method to obtain user by ID
 */
export const getUserByID = async (id: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity();
    // Search user by ID
    return await userModel.findById(id);
  } catch (error) {
    LogError(`[ORM ERROR]: Getting user by ID. ${error}`);
  }
};

/**
 * Method to delete user by ID
 */
export const deleteUserByID = async (id: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity();
    // Delete user by ID
    return await userModel.deleteOne({ _id: id });
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user by ID. ${error}`);
  }
};

/**
 * Method to create new user
 */
export const createNewUser = async (
  user: IUser
): Promise<IUser | undefined> => {
  try {
    const userModel = userEntity();
    // Create new user
    return await userModel.create(user);
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user. ${error}`);
  }
};

/**
 * Method to update user
 */
export const updateUser = async (
  id: string,
  user: IUser
): Promise<IUser | null | undefined> => {
  try {
    const userModel = userEntity();
    // Update user
    return await userModel.findByIdAndUpdate(id, user, { new: true });
  } catch (error) {
    LogError(`[ORM ERROR]: Updating user ${id}. ${error}`);
  }
};

/**
 * Method to create new user
 */
export const registerUser = async (user: IUser): Promise<IUser | undefined> => {
  try {
    const userModel = userEntity();
    // Create new user
    return await userModel.create(user);
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user. ${error}`);
  }
};

// TODO
// - Get user by email
// - Login user
