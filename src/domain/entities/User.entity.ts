import { Schema, model, models } from 'mongoose';
import { IUser } from '../interfaces/IUser.interface';

export const userEntity = () => {
  const userSchema = new Schema<IUser>({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    age: { type: Number, required: true }
  });

  return models.Users || model<IUser>('Users', userSchema);
};
