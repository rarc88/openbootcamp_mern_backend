import express, { Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import { AuthController } from '../controllers/AuthController';
import { IUser } from '../domain/interfaces/IUser.interface';
import { IAuth } from '../domain/interfaces/IAuth.interface';

// BCRYPT for passwords
import bcrypt from 'bcrypt';

// Router from express
const authRouter = express.Router();

authRouter.route('/auth/register').post(async (req: Request, res: Response) => {
  // Obtain the body
  const body: IUser = req?.body;
  LogInfo(`Body: ${JSON.stringify(body)}`);

  // Validate body

  // Obtain the password in request and cypher
  body.password = bcrypt.hashSync(body.password, 8);
  // Controller instance to execute method
  const controller: AuthController = new AuthController();
  // Obtain response
  const response = await controller.registerUser(body);
  // Send to the client the resposne
  res.status(201).send(response);
});

authRouter.route('/auth/login').post(async (req: Request, res: Response) => {
  // Obtain the body
  const body: IAuth = req?.body;
  LogInfo(`Body: ${JSON.stringify(body)}`);

  // Validate body

  // Controller instance to execute method
  const controller: AuthController = new AuthController();
  // Obtain response
  const response = await controller.loginUser(body);
  // Send to the client the resposne which includes the JWT to authorize requests
  res.status(201).send(response);
});

export default authRouter;
