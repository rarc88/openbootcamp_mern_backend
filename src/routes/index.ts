/**
 * Root router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import authRouter from './AuthRouter';
import helloRouter from './HelloRouter';
import userRouter from './UserRouter';

// Server instance
const server = express();

// Router instance
const rootRouter = express.Router();

// Activate for requests /api
rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: /api');
  res.send('Express server + TS + Jest + Swagger + Mongoose');
});

// Redirections to routers & controllers
server.use('/', rootRouter);
server.use('/hello', helloRouter);
server.use('/users', userRouter);
// Auth routes
server.use('/auth', authRouter);

export default server;
