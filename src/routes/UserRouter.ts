import express, { Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import { UserController } from '../controllers/UserController';
import { IUser } from '@/domain/interfaces/IUser.interface';

// Router from express
const userRouter = express.Router();

userRouter
  .route('/')
  // GET -> /api/users?id=63cf1c752bba9318cf8281be
  .get(async (req: Request, res: Response) => {
    // Obtain a query param
    const id: any = req?.query?.id;
    LogInfo(`Query param: ${id}`);
    // Controller instance to execute method
    const controller: UserController = new UserController();
    // Obtain response
    const response = await controller.getUsers(id);
    // Send to the client the resposne
    res.status(200).send(response);
  })
  .delete(async (req: Request, res: Response) => {
    // Obtain a query param
    const id: any = req?.query?.id;
    LogInfo(`Query param: ${id}`);
    // Controller instance to execute method
    const controller: UserController = new UserController();
    // Obtain response
    const response = await controller.deleteUser(id);
    // Send to the client the resposne
    res.status(response.status).send(response);
  })
  .post(async (req: Request, res: Response) => {
    // Obtain the body
    const body: IUser = req?.body;
    LogInfo(`Body: ${JSON.stringify(body)}`);
    // Controller instance to execute method
    const controller: UserController = new UserController();
    // Obtain response
    const response = await controller.createUser(body);
    // Send to the client the resposne
    res.status(201).send(response);
  })
  .put(async (req: Request, res: Response) => {
    // Obtain a query param
    const id: any = req?.query?.id;
    LogInfo(`Query param: ${id}`);
    // Obtain the body
    const body: IUser = req?.body;
    LogInfo(`Body: ${JSON.stringify(body)}`);
    // Controller instance to execute method
    const controller: UserController = new UserController();
    // Obtain response
    const response = await controller.updateUser(body, id);
    // Send to the client the resposne
    res.status(response.status).send(response);
  });

export default userRouter;
