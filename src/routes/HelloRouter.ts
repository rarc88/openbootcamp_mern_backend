import express, { Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import { HelloController } from '../controllers/HelloController';

// Router from express
const helloRouter = express.Router();

helloRouter
  .route('/')
  // GET -> /api/hello?name=Roberto
  .get(async (req: Request, res: Response) => {
    // Obtain a query param
    const name: any = req?.query?.name;
    LogInfo(`Query param: ${name}`);
    // Controller instance to execute method
    const controller: HelloController = new HelloController();
    // Obtain response
    const response = await controller.getMessage(name);
    // Send to the client the resposne
    res.send(response);
  });

export default helloRouter;
