import { IUser } from '@/domain/interfaces/IUser.interface';
import { BasicResponse } from '../types';

export interface IHelloController {
  getMessage(name?: string): Promise<BasicResponse>;
}

export interface IUserController {
  // Get all users from database || Get user by ID
  getUsers(id?: string): Promise<any>;
  // Delete user by ID
  deleteUser(id?: string): Promise<any>;
  // Create new user
  createUser(user: IUser): Promise<any>;
  // Update user
  updateUser(user: IUser, id?: string): Promise<any>;
}

export interface IAuthController {
  // Register new user
  registerUser(user: IUser): Promise<any>;
  // Login user
  loginUser(auth: any): Promise<any>;
}
