import { Post, Route, Tags } from 'tsoa';
import { LogSuccess } from '../utils/logger';
import { IAuth } from '../domain/interfaces/IAuth.interface';
import { IUser } from '../domain/interfaces/IUser.interface';
import { IAuthController } from './interfaces';

// ORM - Users collection
import * as authOrm from '../domain/orm/Auth.orm';

@Route('/auth/')
@Tags('AuthController')
export class AuthController implements IAuthController {
  /**
   *
   * @param user
   */
  @Post('/register')
  public async registerUser(user: IUser): Promise<any> {
    LogSuccess(`[/auth/register] Register new user: ${JSON.stringify(user)}`);
    return await authOrm.registerUser(user);
  }

  /**
   *
   * @param auth
   */
  @Post('/login')
  public async loginUser(auth: IAuth): Promise<any> {
    LogSuccess(`[/auth/login] Logged in user: ${JSON.stringify(auth.email)}`);
    return await authOrm.loginUser(auth);
  }

  /**
   *
   * @param user
   */
  @Post('/logout')
  logout(user: IUser): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
