import { Body, Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa';
import { LogSuccess, LogWarning } from '../utils/logger';
import { IUserController } from './interfaces';
import { IUser } from '@/domain/interfaces/IUser.interface';

// ORM - Users collection
import * as userOrm from '../domain/orm/User.orm';

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Endpoint to retreive the users in the collection "Users" of DB
   * @param {string} id Id of user to retreive (optional)
   * @returns All user o user found by ID
   */
  @Get('/')
  public async getUsers(@Query() id?: string): Promise<any> {
    if (id) {
      LogSuccess('[/api/users] Get user by ID request');
      return await userOrm.getUserByID(id);
    } else {
      LogSuccess('[/api/users] Get all users request');
      return await userOrm.getAllUsers();
    }
  }

  /**
   * Endpoint to delete user by ID in the collection "Users" of DB
   * @param {string} id Id of user to retreive (optional)
   * @returns message informing if deletion was correct
   */
  @Delete('/')
  public async deleteUser(@Query() id?: string): Promise<any> {
    const data = {
      status: 400,
      message: ''
    };
    if (id) {
      LogSuccess('[/api/users] Delete user by ID request');
      const result = await userOrm.deleteUserByID(id);
      if (result?.deletedCount === 1) {
        data.status = 200;
        data.message = `User with id ${id} deleted successfully`;
      } else {
        data.message = `Error to try delete user with id ${id}`;
      }
    } else {
      LogWarning('[/api/users] Delete user request WITHOUT ID');
      data.message = 'Please, provide an ID to remove from database';
    }
    return data;
  }

  /**
   * Endpoint to create users in the collection "Users" of DB
   * @param {any} user Body
   * @returns User object
   */
  @Post()
  public async createUser(@Body() user: IUser): Promise<IUser | undefined> {
    LogSuccess(`[/api/users] Create new user: ${JSON.stringify(user)}`);
    return await userOrm.createNewUser(user);
  }

  /**
   * Endpoint to update user by ID in the collection "Users" of DB
   * @param {any} user Body
   * @returns User object
   */
  @Put('/')
  public async updateUser(
    @Body() user: IUser,
    @Query() id?: string
  ): Promise<any> {
    const data = {
      status: 200,
      message: ''
    };
    if (id) {
      LogSuccess('[/api/users] Update user by ID request');
      const result = await userOrm.updateUser(id, user);
      // if (result?.deletedCount === 1) {
      //   message = `User with id ${id} updated successfully`;
      // } else {
      //   message = `Error to try update user with id ${id}`;
      // }
      return result;
    } else {
      LogWarning('[/api/users] Update user request WITHOUT ID');
      data.status = 400;
      data.message = 'Please, provide an ID to update from database';
    }
    return data;
  }
}
