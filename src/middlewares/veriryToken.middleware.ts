import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

/**
 *
 * @param { Request } req Original request previous middleware of verification JWT
 * @param { Response } res Response to verification of JWT
 * @param { NextFunction } next Next function to be executed
 * @returns Errors of verification or next execution
 */
export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Check HEADER from Request for 'x-access-token'
  const token: any = req.headers['x-access-token'];

  // Verify if jwt is present
  if (!token) {
    return res.status(403).send({
      status: 403,
      messaje: 'Not authorised to consume this endpoint'
    });
  }

  // Verify the token obtained
  jwt.verify(token, '', (error: any, decoded: any) => {
    if (error) {
      return res.status(500).send({
        status: 500,
        message: 'Feailed to verify JWT in request'
      });
    }

    // Pass something to next request

    // Execute next function, protected routes wil be executed
    next();
  });
};
