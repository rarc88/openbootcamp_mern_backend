import dotenv from 'dotenv';
import { LogError, LogSuccess } from './src/utils/logger';

// Configuration the .env file
dotenv.config();

// Loading server configuration
import server from './src/server';

const port: number = Number(process.env.PORT || 3000);

// Execute server and listen request to port
server.listen(port, () => {
  LogSuccess(`Express server running at http://localhost:${port}`);
});

// Control server error
server.on('error', (error: any) => {
  LogError(error);
});
